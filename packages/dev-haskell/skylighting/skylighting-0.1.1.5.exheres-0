# Copyright 2017 Marc-Antoine Perennou <keruspe@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require hackage [ has_bin="true" ]

SUMMARY="A syntax highlighting library with support for nearly one hundred languages."
DESCRIPTION="Skylighting is a syntax highlighting library with support for over one hundred
languages. It derives its tokenizers from XML syntax definitions used by KDE's KSyntaxHighlighting
framework, so any syntax supported by that framework can be added. An optional command-line program
is provided. Skylighting is intended to be the successor to highlighting-kate."

LICENCES="GPL-2"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/aeson
        dev-haskell/blaze-html[>=0.5]
        dev-haskell/bytestring
        dev-haskell/case-insensitive
        dev-haskell/containers
        dev-haskell/directory
        dev-haskell/filepath
        dev-haskell/hxt
        dev-haskell/mtl
        dev-haskell/pretty-show
        dev-haskell/regex-pcre-builtin
        dev-haskell/safe
        dev-haskell/text
        dev-haskell/utf8-string
    ")
    $(haskell_test_dependencies "
        dev-haskell/Diff
        dev-haskell/tasty
        dev-haskell/tasty-golden
        dev-haskell/tasty-hunit
    ")
"

CABAL_SRC_CONFIGURE_PARAMS=(
    --flags=-bootstrap
    --flags=executable
)

src_install() {
    cabal_src_install

    alternatives_for ${PN} ${SLOT} 1 \
        /usr/$(exhost --target)/bin/skylighting skylighting-${SLOT}
}

RESTRICT="test"

