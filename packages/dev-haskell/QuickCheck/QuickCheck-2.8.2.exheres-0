# Copyright 2008 Santiago M. Mola
# Copyright 2009, 2010 Ingmar Vanhassel
# Distributed under the terms of the GNU General Public License v2

require hackage [ ghc_dep=">=7" ]

SUMMARY="An automatic, specification based testing utility for Haskell programs"
DESCRIPTION="
QuickCheck is a library for random testing of program properties.

The programmer provides a specification of the program, in the form of
properties which functions should satisfy, and QuickCheck then tests that the
properties hold in a large number of randomly generated cases...

Specifications are expressed in Haskell, using combinators defined in the
QuickCheck library. QuickCheck provides combinators to define properties,
observe the distribution of test data, and define test data generators.
"
HOMEPAGE="http://www.cs.chalmers.se/~koen/ ${HOMEPAGE}"

LICENCES="BSD-3"
PLATFORMS="~amd64 ~x86"
MYOPTIONS=""
# break annoying dependency cicle
RESTRICT="test"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/containers
        dev-haskell/random
        dev-haskell/template-haskell[>=2.4]
        dev-haskell/tf-random[>=0.4]
        dev-haskell/transformers[>=0.2]
    ")
    $(haskell_test_dependencies "
        dev-haskell/containers
        dev-haskell/template-haskell[>=2.4]
        dev-haskell/test-framework[>=0.4&<0.9]
    ")
"

CABAL_SRC_CONFIGURE_PARAMS=(
    "--flags=templateHaskell"
)

