# Copyright 2015 Mykola Orliuk <virkony@gmail.com>
# Distributed under the terms of the GNU General Public License v2

require hackage [ has_bin=true ]

SUMMARY="Interface to some of Cabal's configuration state used by ghc-mod"
DESCRIPTION="
cabal-helper uses a wrapper executable to compile the actual cabal-helper
executable at runtime while linking against an arbitrary version of Cabal. This
runtime-compiled helper executable is then used to extract various bits and
peices from Cabal's on disk state (dist/setup-config) written by it's configure
command.
"

LICENCES="AGPL-3"
PLATFORMS="~amd64"

DEPENDENCIES="
    $(haskell_lib_dependencies "
        dev-haskell/Cabal[>=1.14&<1.26]
        dev-haskell/directory
        dev-haskell/filepath
        dev-haskell/mtl
        dev-haskell/process
        dev-haskell/transformers
    ")
    $(haskell_test_dependencies "
        dev-haskell/Cabal[>=1.14&<1.26]
        dev-haskell/bytestring
        dev-haskell/directory
        dev-haskell/filepath
        dev-haskell/mtl
        dev-haskell/process
        dev-haskell/template-haskell
        dev-haskell/temporary
        dev-haskell/transformers
        dev-haskell/utf8-string
    ")
    $(haskell_bin_dependencies "
        dev-haskell/Cabal[>=1.14&<1.26]
        dev-haskell/bytestring
        dev-haskell/directory
        dev-haskell/filepath
        dev-haskell/process
        dev-haskell/template-haskell
        dev-haskell/temporary
        dev-haskell/transformers
        dev-haskell/utf8-string
    ")
"

# tests fails
RESTRICT=test

src_install() {
    cabal_src_install
    alternatives_for _${PN} ${SLOT} ${PV} \
        /usr/$(exhost --target)/libexec/cabal-helper-wrapper cabal-helper-wrapper-${SLOT}
}

